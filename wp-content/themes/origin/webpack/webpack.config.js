const path = require("path");
const miniCssExtractPlugin = require("mini-css-extract-plugin");
const webpack = require("webpack");

/**
 * Module's
 * @type {{test: RegExp, exclude: RegExp, use: {loader: string, options: {presets: string[], plugins: string[]}}}}
 */
const javascriptRules = {
  test: /\.js$/,
  exclude: /node_modules/,
  use: {
    loader: "babel-loader",
    options: {
      presets: ["@babel/preset-react", "@babel/preset-env"],
      plugins: ["@babel/plugin-proposal-optional-chaining"],
    },
  },
};

const imageRules = {
  test: /\.(jpg|png|git|jpeg)$/,
  use: [
    {
      loader: "file-loader",
      options: {
        name: "[name].[ext]",
        outputPath: "static/",
        useRelativePath: true,
      },
    },
  ],
};

module.exports = {
  entry: "./src/js/app.js",
  stats: {
    children: true,
    errorDetails: true,
  },
  output: {
    path: path.resolve(__dirname, "../dist"),
    filename: "js/app.js",
  },
  module: {
    rules: [javascriptRules, imageRules],
  },
};
