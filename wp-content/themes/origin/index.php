<?= get_header() ?>
    <div class="container my-5">
        <h1 class="pt-5"><?php bloginfo( 'name' ); ?></h1>
        <label class="text-muted"><?php bloginfo( 'description' ); ?></label>

        <div class="border-search border-radius-8 p-3 my-5">
			<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

                <h3><?php the_title(); ?></h3>

				<?php the_content(); ?>
				<?php wp_link_pages(); ?>
				<?php edit_post_link(); ?>

			<?php endwhile; ?>

				<?php
				if ( get_next_posts_link() ) {
					next_posts_link();
				}
				?>
				<?php
				if ( get_previous_posts_link() ) {
					previous_posts_link();
				}
				?>

			<?php else: ?>


                <p>No existe ningun contenido</p>


			<?php endif; ?>
        </div>
    </div>


<?= get_footer() ?>