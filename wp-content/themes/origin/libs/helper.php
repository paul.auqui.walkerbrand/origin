<?php
/**
 * Created by PhpStorm.
 * User: ZBOOK
 * Date: 05/05/2021
 * Time: 11:24
 */

class helper {
	public function getPathMeta( $part, $post_id = null ) {
		$info    = [];
		$post_id = $post_id ? $post_id : get_the_ID();

		$group = get_post_meta( $post_id ) ?? [];
		foreach ( $group as $key => $meta ) {
			if ( strpos( $key, $part ) !== false ) {
				$key_ = str_replace( $part . '_', '', $key );
				if ( substr( $key_, 0, 1 ) !== '_' ) {
					$info[ $key_ ] = ( count( $meta ) == 1 ) ? $meta[0] : $meta;

					if ( $key_ == 'image' ) {
						$info['image_link'] = $this->getImageMeta( $post_id, $key, 'full' );
					}
				}
			}
		}

		return ( $info ) ? (object) $info : null;
	}

	public function getGroupMeta( $part, $post_id = null ) {
		$group = [];
		for ( $i = 1; ; $i ++ ) {
			$node = $this->getPathMeta( "{$part}_{$i}", $post_id );

			if ( ! $node ) {
				break;
			}

			$group[ $i ] = $node;
		}

		return $group;
	}

	public function getImageMeta( $post_id, $key, $size ) {
		if ( $img = wp_get_attachment_image_src( get_post_meta( $post_id, $key, true ), $size ) ) {
			return $img[0];
		}

		return null;
	}

	/**
	 * @param WP_Term|integer $term
	 * @param string $img
	 * @param string $size
	 *
	 * @return string|null
	 */
	public function getImageTermMeta( $term, $img, $size ) {
		$term_id  = is_object( $term ) ? $term->term_id : $term;
		$tempMeta = get_term_meta( $term_id, $img, true );

		if ( $img = wp_get_attachment_image_src( $tempMeta, $size ) ) {
			return $img[0];
		}

		return null;
	}

	public function getPostTypeMenuSeguros( $post_id = 0 ) {
		$seguros_ = [];
		$seguros  = new WP_Query( [
			'post_type'      => 'seguro',
			'posts_per_page' => - 1,
			'post_parent'    => $post_id,
			'orderby'        => 'menu_order',
			'order'          => 'ASC',
		] );

		if ( $seguros->have_posts() ) {
			while ( $seguros->have_posts() ) {
				$seguros->the_post();
				$seguros_[] = $this->getAttributesMenuSeguros( get_the_ID() );
			}
		}

		wp_reset_postdata();

		foreach ( $seguros_ as $key => $seguros__ ) {
			$parent_       = [];
			$segurosParent = new WP_Query( [
				'post_type'      => 'seguro',
				'posts_per_page' => - 1,
				'post_parent'    => $seguros__->id,
				'orderby'        => 'menu_order',
				'order'          => 'ASC',
			] );

			if ( $segurosParent->have_posts() ) {
				while ( $segurosParent->have_posts() ) {
					$segurosParent->the_post();
					$parent_[] = $this->getAttributesMenuSeguros( get_the_ID() );
				}
			}

			$seguros_[ $key ]->parent_count = count( $parent_ );
			$seguros_[ $key ]->parent       = $parent_;
		}

		return $seguros_;
	}

	public function getAttributesMenuSeguros( $post_id ) {
		$img_principal        = $this->getImageMeta( $post_id, 'icono_principal', 'full' );
		$img_principal_select = $this->getImageMeta( $post_id, 'icono_principal_select', 'full' );
		$img                  = $this->getImageMeta( $post_id, 'icono_menu', 'full' );
		$img_select           = $this->getImageMeta( $post_id, 'icono_menu_select', 'full' );
		$color                = get_post_meta( $post_id, 'color_menu', true );
		$color_shadow         = get_post_meta( $post_id, 'color_shadow_menu', true );
		$default              = get_post_meta( $post_id, 'default_menu', true );

		$post_ = (object) [
			'id'   => $post_id,
			'name' => get_the_title(),
			'slug' => get_post_field( 'post_name', $post_id ),
			'link' => get_the_permalink(),
			'meta' => (object) [
				'icono_principal'        => $img_principal,
				'icono_principal_select' => $img_principal_select,
				'icono'                  => $img,
				'icono_select'           => $img_select,
				'color'                  => $color,
				'color_shadow'           => $color_shadow,
				'default'                => $default,
			]
		];

		return $post_;
	}

	public function getTermsMenuTipo() {
		$terms_       = [];
		$termsPrimary = get_terms( array(
				'taxonomy'   => 'tipo',
				'hide_empty' => false,
				'parent'     => 0,
			) ) ?? [];

		foreach ( $termsPrimary as $termPrimary ) {
			$parent  = [];
			$img     = $this->getImageTermMeta( $termPrimary, 'image', 'full' ) ?? null;
			$color   = get_term_meta( $termPrimary->term_id, 'color', true );
			$default = get_term_meta( $termPrimary->term_id, 'default', true );

			$terms__       = $termPrimary;
			$terms__->meta = (object) [
				'color'   => $color,
				'default' => $default,
				'image'   => $img
			];

			$termChilds = get_terms( array(
					'taxonomy'   => 'tipo',
					'hide_empty' => false,
					'parent'     => $termPrimary->term_id,
				) ) ?? [];

			if ( $termChilds ) {
				foreach ( $termChilds as $termChild ) {
					$img     = $this->getImageTermMeta( $termChild, 'image', 'full' ) ?? null;
					$default = get_term_meta( $termChild->term_id, 'default', true ) ?? null;
					$color   = get_term_meta( $termChild->term_id, 'color', true ) ?? null;

					$terms___       = $termChild;
					$terms___->meta = (object) [
						'color'   => $color,
						'default' => $default,
						'image'   => $img
					];

					$parent[] = $terms___;
				}
			}

			$terms__->parent = $parent;
			$terms_[]        = $terms__;
		}

		return $terms_;
	}

	public function getSumParamObject( $array, $term ) {
		$map = function ( $term ) {
			$parent_count = 'parent_count';
			$value        = is_object( $term ) ? ( $term->{$parent_count} ) : $term[ $parent_count ];

			return (int) $value;
		};

		$count  = array_count_values( array_map( $map, $array ) );
		$values = array_keys( $count );

		return array_sum( $values );
	}

	public function getLevelParent( $post_id ) {
		$level   = 1;
		$post_id = ( is_object( $post_id ) ) ? $post_id->ID : $post_id;

		$post_id = wp_get_post_parent_id( $post_id );
		$level   = ( $post_id > 0 ) ? ( $level + 1 ) : $level;
		if ( $post_id == 0 ) {
			return $level;
		}


		$post_id = wp_get_post_parent_id( $post_id );
		$level   = ( $post_id > 0 ) ? ( $level + 1 ) : $level;
		if ( $post_id == 0 ) {
			return $level;
		}

		$post_id = wp_get_post_parent_id( $post_id );
		$level   = ( $post_id > 0 ) ? ( $level + 1 ) : $level;
		if ( $post_id == 0 ) {
			return $level;
		}

	}

	public function getPostTypeEmpresas() {
		$data     = [];
		$empresas = new WP_Query( [
			'post_type'      => 'empresa',
			'posts_per_page' => - 1,
			'orderby'        => 'menu_order',
			'order'          => 'ASC',
		] );

		if ( $empresas->have_posts() ) {
			while ( $empresas->have_posts() ) {
				$empresas->the_post();
				$image = $this->getImageMeta( get_the_ID(), 'image', 'full' );
				//d( get_post_meta( get_the_ID() ) );

				$data[] = (object) [
					'title'      => get_the_title(),
					'image_link' => $image,
					'link'       => get_post_meta( get_the_ID(), 'link', true )
				];
			}
		}

		return $data;
	}

	public function getPostTypeVacantes() {
		$data     = [];
		$vacantes = new WP_Query( [
			'post_type'      => 'vacante',
			'posts_per_page' => - 1,
			'orderby'        => 'menu_order',
			'order'          => 'ASC',
		] );

		if ( $vacantes->have_posts() ) {
			while ( $vacantes->have_posts() ) {
				$vacantes->the_post();
				$anuncio = get_post_meta( get_the_ID(), 'anuncio', true );
				$ciudad  = get_post_meta( get_the_ID(), 'ciudad', true );

				$data[] = (object) [
					'title'   => get_the_title(),
					'anuncio' => $anuncio,
					'ciudad'  => $ciudad,
					'link'    => get_post_permalink( get_the_ID() )
				];
			}
		}

		return $data;
	}

	public function getPostTypeTimeLines() {
		$data      = [];
		$timelines = new WP_Query( [
			'post_type'      => 'time-line',
			'posts_per_page' => - 1,
			'orderby'        => 'menu_order',
			'order'          => 'ASC',
		] );

		if ( $timelines->have_posts() ) {
			while ( $timelines->have_posts() ) {
				$timelines->the_post();
				$anio        = get_post_meta( get_the_ID(), 'ano', true );
				$descripcion = get_post_meta( get_the_ID(), 'descripcion', true );
				$posicion    = get_post_meta( get_the_ID(), 'posicion', true );
				$data[]      = (object) [
					'title'       => get_the_title(),
					'anio'        => $anio,
					'descripcion' => $descripcion,
					'posicion'    => $posicion,
					'link'        => get_post_permalink( get_the_ID() )
				];
			}
		}

		return $data;
	}

	public function getPostTypeTestimonial() {
		$data     = [];
		$vacantes = new WP_Query( [
			'post_type'      => 'tertimonial',
			'posts_per_page' => - 1,
			'orderby'        => 'menu_order',
			'order'          => 'ASC',
		] );

		if ( $vacantes->have_posts() ) {
			while ( $vacantes->have_posts() ) {
				$vacantes->the_post();

				$data[] = (object) [
					'ID'      => get_the_ID(),
					'title'   => get_the_title(),
					'content' => get_the_content(),
					'link'    => get_post_permalink( get_the_ID() ),
					'image'   => get_the_post_thumbnail_url(),
					'color'   => get_post_meta( get_the_ID(), 'color', true )
				];
			}
		}

		wp_reset_postdata();

		return $data;
	}

	public function format_date( $date, $format ) {
		return date_format( date_create( $date ), $format );
	}

	public function getHtmlPartial( $params, $path ) {
		$postdata = http_build_query( $params );
		$opts     = [
			'http' =>
				[
					'method'  => 'POST',
					'header'  => 'Content-Type: application/x-www-form-urlencoded',
					'content' => $postdata
				]
		];

		// dd($postdata, $params);

		$context = stream_context_create( $opts );
		$content = file_get_contents( $path, false, $context );

		return $content ?? null;
	}

	public function getMailVacanteCiudad( $ciudad ) {
		$ciudad = strtolower( $ciudad );
		switch ( $ciudad ) {
			case 'quito':
				return DEV ? 'paul.auqui@walkerbrand.com' : 'cron@tecniseguros.com.ec';
				break;
			case 'guayaquil':
				return DEV ? 'paul.auqui@walkerbrand.com' : 'arocha@tecniseguros.com.ec';
				break;
			case 'cuenca':
				return DEV ? 'paul.auqui@walkerbrand.com' : 'cron@tecniseguros.com.ec';
				break;
			case 'santo domingo':
				return DEV ? 'paul.auqui@walkerbrand.com' : 'arocha@tecniseguros.com.ec';
				break;
			case 'ambato':
				return DEV ? 'paul.auqui@walkerbrand.com' : 'cron@tecniseguros.com.ec';
				break;
			case 'manta':
				return DEV ? 'paul.auqui@walkerbrand.com' : 'arocha@tecniseguros.com.ec';
				break;
			default:
				return DEV ? 'paul.auqui@walkerbrand.com' : 'cron@tecniseguros.com.ec';
		}
	}

	public function getPostsSearchType() {
		$data = [];
		if ( have_posts() ) {
			while ( have_posts() ) {
				the_post();
				$data[ get_post_type() ]['parent']   = [
					'link' => get_the_permalink( get_post_parent() ),
					'post' => get_post_parent()
				];
				$data[ get_post_type() ]['search'][] = [
					'link' => get_the_permalink(),
					'post' => get_post()
				];
			}
		}

		return $data;
	}

	public function getPostTypeOficinas() {
		$data     = [];
		$oficinas = new WP_Query( [
			'post_type'      => 'oficina',
			'posts_per_page' => - 1,
			'orderby'        => 'menu_order',
			'order'          => 'ASC',
		] );

		if ( $oficinas->have_posts() ) {
			while ( $oficinas->have_posts() ) {
				$oficinas->the_post();
				$telefono = get_post_meta( get_the_ID(), 'telefono', true );
				$latitud  = get_post_meta( get_the_ID(), 'latitud', true );
				$longitud = get_post_meta( get_the_ID(), 'longitud', true );
				$default  = get_post_meta( get_the_ID(), 'default', true );

				$data[] = (object) [
					'title'     => get_the_title(),
					'direccion' => get_the_content(),
					'telefono'  => $telefono,
					'latitud'   => $latitud,
					'longitud'  => $longitud,
					'default'   => $default
				];
			}
		}

		wp_reset_postdata();

		return $data;
	}

	/**
	 * @param WP_Post $seguro
	 * @param $url
	 */
	public function createLinkParam( $name = null, $param, $url ) {
		$query = parse_url( $url, PHP_URL_QUERY );

		$url .= ( $query ) ? '&' : '?';
		$url .= ( $name ) ? "{$name}=" : '';
		$url .= $param;

		return $url;
	}

	public function getContentReplaceField( $content, $params ) {
		$params = (array) $params;
		foreach ( $params as $key => $value ) {
			if ( is_object( $value ) ) {
				$value = (array) $value;
			}

			if ( is_array( $value ) ) {
				$value = ( $key == 'area-seguro' ) ? implode( ', ', $value ?? [] ) : ucwords( implode( ', ', array_keys( $value ) ?? [] ) );
			}

			$search  = "{{{$key}}}";
			$content = str_replace( $search, $value, $content );
		}

		return $content;
	}
}