window.menuHover = function menuHover(classContent) {
    // Verificar si existe un elemento activo para mostrar icono marcado
    $.each($('.' + classContent + ' .nav-item a.active'), function (i, element) {
        if ($(element).find('.icon-select').length) {
            $(element).find('.icon').hide();
            $(element).find('.icon-select').show();
        }
    });

    $('.' + classContent + ' .nav-item a')
        .mouseover(function () {
            // Desactivamos todos los menus
            $('.' + classContent + ' .nav-item a')
                .removeClass('active');

            // Activamos el menu seleccionado
            $(this).addClass('active');

            // Ocultamos todos los iconos hover para mostrar el normal y el hover seleccionado
            $('.' + classContent + ' .icon').show();
            $('.' + classContent + ' .icon-select').hide();
            if ($(this).find('.icon-select').length) {
                $(this).find('.icon').hide();
                $(this).find('.icon-select').show();
            }

            // Obtengo la referencia del panel que necesito mostrar
            var ref = $(this).data('bs-target') // #individual

            // Elimino las clases de show y activo al panel activo
            $('.' + classContent + '.tab-pane')
                .removeClass('active')
                .removeClass('show');

            // Agregar a referencia la clases
            $(ref)
                .addClass('active')
                .addClass('show');
        })
        .mouseout(function () {

        });

    $('.' + classContent + ' .item a')
        .mouseover(function () {
            // Ocultamos todos los iconos hover para mostrar el normal y el hover seleccionado
            if ($(this).find('.icon-select').length) {
                $(this).find('.icon').hide();
                $(this).find('.icon-select').show();
            }
        })
        .mouseout(function () {
            $('.' + classContent + ' .item .icon').show();
            $('.' + classContent + ' .item .icon-select').hide();
        });
};

window.setIconHover = function setIconHover(classContent) {
    $('.' + classContent + ' a')
        .mouseover(function () {
            // Ocultamos todos los iconos hover para mostrar el normal y el hover seleccionado
            if ($(this).find('.icon-select').length) {
                $(this).find('.icon').hide();
                $(this).find('.icon-select').show();
            }
        })
        .mouseout(function () {
            $('.' + classContent + ' .icon').show();
            $('.' + classContent + ' .icon-select').hide();
        });
};

window.alertError = function alertError(msn) {
    window.Swal.fire({
        imageUrl: dcms_vars.templateurl + '/dist/static/logo-tecniseguros.png',
        imageWidth: 350,
        text: msn,
        title: '¡Lo sentimos!',
        showCancelButton: true,
        showConfirmButton: false,
        cancelButtonText: 'Cancelar',
    });
};

window.alertSuccess = function alertSuccess(msn, callbacksuccess, title = '', text_button = '', allowOutsideClick = true) {
    window.Swal.fire({
        imageUrl: dcms_vars.templateurl + '/dist/static/logo-tecniseguros.png',
        imageWidth: 350,
        text: msn,
        title: (!title) ? 'Gracias por dejar tus datos' : title,
        confirmButtonText: (!text_button) ? 'Gracias' : text_button,
        allowOutsideClick: allowOutsideClick
    }).then((result) => {
        if (result.isConfirmed) {
            if (callbacksuccess != undefined) {
                callbacksuccess();
            }
        }
    });
};

window.alertProcess = function alertProcess(msn, delay = 10000) {
    Swal.fire({
        title: '¡Enviando información!',
        html: msn,
        timer: delay,
        timerProgressBar: true,
        didOpen: () => {
            Swal.showLoading()
            timerInterval = setInterval(() => {
                const content = Swal.getHtmlContainer()
                if (content) {
                    const b = content.querySelector('b')
                    if (b) {
                        b.textContent = Swal.getTimerLeft()
                    }
                }
            }, 100)
        },
        willClose: () => {
            clearInterval(timerInterval)
        }
    }).then((result) => {
        /* Read more about handling dismissals below */
        if (result.dismiss === Swal.DismissReason.timer) {
            console.log('I was closed by the timer')
        }
    })
};

window.sendAjax = function sendAjax(params, callback, callbackerror, callbackbefore) {
    params.success = function (resultado) {
        callback(resultado);
    };

    params.error = function (xhr, ajaxOptions, thrownError) {
        if (callbackerror != undefined) {
            callbackerror();
        } else {
            console.log(xhr, ajaxOptions, thrownError);
            var json = $.parseJSON(xhr.responseText);
            console.log(json);
        }
    };


    params.beforeSend = function (response) {
        if (callbackbefore != undefined) {
            callbackbefore(response);
        }
    };

    params.fail = function (data) {
        console.log('fail: ', data.responseText);
    };

    $.ajax(params);
};