/**
 * Bootstrap
 */
// import bootstrap from 'bootstrap';

/**
 * Imagenes
 */
import "./images";
/**
 * Plugins
 */
// import "@fortawesome/fontawesome-free/js/all"

/**
 * Css
 */
// require("../css/app.scss");

import "./helper";

// window.Swal = require('sweetalert2');

import "wow.js";
// wow = new WOW({
//   boxClass: "wow", // default
//   animateClass: "animated", // default
//   offset: 0, // default
//   mobile: true, // default
//   live: true, // default
// }); 
// wow.init();
