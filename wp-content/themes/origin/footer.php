<footer id="colophon" class="bg-primary_900 pt-5 text-white">
    <div class="container-origin">
        <nav class="w-full px-2 sm:px-4 pt-4 ">
            <div class="flex flex-wrap justify-between items-center mx-auto">
                <a href="<?= get_home_url() ?>">
                    <?php dynamic_sidebar( 'footer - logo' ); ?>
                </a>

                <div class="justify-between items-center w-full md:flex md:w-auto md:order-1" id="mobile-menu-2-footer">
                    <?php dynamic_sidebar( 'footer - menu' ); ?>
                </div>

                <div class="flex justify-between items-center redes md:order-2 space-x-2">
                    <?php dynamic_sidebar( 'footer - redessociales' ); ?>
                </div>
            </div>
        </nav>

        <div class="text-center p-8 text-sm text-grey">
            © <a class="link-white" href="https://www.walkerbrand.com/"> Copyright 2022. All Rights Reserved.</a>
        </div>
    </div>
</footer>

<?php wp_footer() ?>
<script>
var menu_principal = document.getElementById("menu-principal");
var img_logo = (img_logo = document.getElementsByClassName("custom-logo")) ? img_logo[0] : null;
var logo_over = "<?= get_stylesheet_directory_uri() ?>/dist/static/logo-blue.png";
var logo_normal = img_logo.src;

var scrollToTop = window.setInterval(function() {
    var pos = window.pageYOffset;
    if (pos > 150) {
        menu_principal.classList.add("top-scroll");
        img_logo.src = logo_over;
    } else {
        menu_principal.classList.remove('top-scroll');
        img_logo.src = logo_normal;
    }
}, 16); // how fast to scroll (this equals roughly 60 fps)
</script>
</body>

</html>