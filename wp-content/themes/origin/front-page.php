<?= get_header() ?>

<section class="w-full h-screen bg-primary_700">
    <div class="w-full h-full lg:bg-home_2 lg:bg-cover lg:bg-center">
        <img class="lg:hidden" src="<?= get_stylesheet_directory_uri() ?>/dist/static/02-back-web.png" alt="Origin">
        <div class="container-origin h-full flex items-start justify-center flex-col">
            <div class="lg:w-2/3 xl:w-2/4 text-white">
                <div class="text-6xl font-bold my-6 wow slideInLeft">
                    El poder de uno.
                </div>
                <div class="text-5xl my-6 wow slideInLeft">
                    Paso a paso, gota a gota y botella a botella.
                </div>
                <div class="">
                    Preservar el bienestar de nuestro planeta y potenciar la cadena de valor de la economía circular
                    inclusiva, es nuestro principal objetivo. Por ese motivo, hace 10 años incursionamos en el mundo del
                    reciclaje de PET post consumo para la fabricación de nuevos productos.
                </div>
                <button class="btn-ver-mas my-4 w-32 p-2 border-2 border-white">
                    Ver más
                </button>
            </div>
        </div>
    </div>
</section>

<section class="bg-white">

    <div class="container-origin py-24">
        <div class="flex ">
            <div class="w-2/4 pr-10">
                <p class="text-5xl font-bold ">
                    El poder de cambiar el presente y transformar el futuro se encuentra en tus manos.
                </p>
                <p class="">
                    Somos la primera marca ecuatoriana que engloba todo el proceso de economía circular inclusiva. Nos
                    encargamos de promover la industrialización participativa y sostenible, mediante la incorporación de
                    recicladores base a nuestra cadena de valor; con la intención de formalizar e incrementar la
                    competitividad de su oficio y obteniendo como consecuencia el aumento de su productividad.
                </p>
                <p class="">
                    Trabajamos con 100 gestores de residuos de Quito en el proceso de recolección, acopio y
                    abastecimiento de PET como materia prima. Actualmente, contamos con una capacidad de acopio de mil
                    toneladas de PET por mes.

                </p>
            </div>
            <div>
                <div class="image">
                    <img class="rounded-2xl" src="<?= get_stylesheet_directory_uri() ?>/dist/static/section_1_2.png"
                        alt="Section_1">
                </div>
            </div>
        </div>
        <div class="grid grid-cols-2 gap-8 mt-10">
            <div class="p-6 rounded-xl bg-light_grey">
                <p class="text-6xl font-bold">+5.000</p>
                <p class=" font-bold">empleos generados</p>
                <p class="pr-8">
                    A recicladores base, han sido registrados hasta este 2022.
                </p>
            </div>
            <div class="p-6 rounded-xl bg-light_grey">
                <p class="text-6xl font-bold">+2.000.000</p>
                <p class=" font-bold">millones</p>
                <p class=" font-bold">De unidades de preformas producidas por día</p>
                <p class="pr-8">
                    Actualmente tenemos el 20% de nuestra capacidad instalada para la fabricación de otras
                    proformas para futuros clientes.
                </p>
            </div>
        </div>
    </div>
</section>

<section class="bg-bg_section_estadistica bg-cover">
    <div class="container-origin py-20">
        <div class="font-bold text-5xl">
            Estadísticas generales
        </div>

        <p class="py-3">
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Auctor augue quis morbi aenean ac placerat arcu
            nec.
        </p>

        <div class="grid grid-cols-4 gap-4 my-5">
            <div class="box-estadistica">
                <span class="font-bold">420+</span>
                <div class="text-xl">botellas recopiladas</div>
            </div>
            <div class="box-estadistica">
                <span class="font-bold">420+</span>
                <div class="text-xl">actores</div>
            </div>
            <div class="box-estadistica">
                <span class="font-bold">420+</span>
                <div class="text-xl">ahorro generado</div>
            </div>
            <div class="box-estadistica">
                <span class="font-bold">420+</span>
                <div class="text-xl">huella de carbono</div>
            </div>
        </div>

        <div class="border border-black flex w-full rounded-xl p-8 justify-between items-center">
            <div>
                <p class="font-bold text-2xl">Conoce toda la información sobre nuestras estadisticas</p>
                <p class="text-dark_grey">
                    Mauris commodo quis imperdiet massa tincidunt nunc. Est ante in nibh mauris cursus mattis molestie.
                </p>
            </div>

            <button class="btn-ver-mas w-32 p-2">Ver más</button>
        </div>
    </div>
</section>

<section class="bg-white">
    <div class="container-origin py-20">
        <div class="font-bold text-5xl">Estadísticas de recolección por origen</div>
        <p class="text-dark_grey my-3">
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Auctor augue quis morbi aenean ac placerat arcu
            nec.
        </p>
        <div class="grid grid-cols-3 gap-6 my-5 text-white">
            <!-- <div class="h-estadistica bg-urban">
                <div class="disable">
                    <img src="<?= get_stylesheet_directory_uri() ?>/dist/static/basurero.png" alt="Section_1">
                    <span class="font-bold uppercase text-6xl py-3">420+</span>
                </div>
            </div> -->
            <div class="h-estadistica bg-oceano">
                <div class="font-bold uppercase text-2xl">costa</div>
            </div>
            <div class="h-estadistica bg-bosque ">
                <div class="font-bold uppercase text-2xl">sierra</div>
            </div>
            <div class="h-estadistica bg-playa">
                <div class="font-bold uppercase text-2xl">amazonía</div>
            </div>
        </div>
    </div>
</section>

<section class="bg-light_grey">
    <div class="container-origin py-20">
        <div class="text-5xl font-bold">Socios</div>

        <div class="flex justify-between items-center py-14">
            <img src="<?= get_stylesheet_directory_uri() ?>/dist/static/socios_1.png" alt="Socio 1">
            <img src="<?= get_stylesheet_directory_uri() ?>/dist/static/socios_2.png" alt="Socio 1">
            <img src="<?= get_stylesheet_directory_uri() ?>/dist/static/socios_3.png" alt="Socio 1">
            <img src="<?= get_stylesheet_directory_uri() ?>/dist/static/socios_4.png" alt="Socio 1">
        </div>
        <p>
            Cada vez son más las empresas y personas que están viviendo un despertar en su conciencia y se suman a este
            movimiento. Recordemos que cuidar el planeta es un esfuerzo en el que todos,debemos hacer nuestra parte.
            <a href="#" class="text-primary_500 hover:text-primary_800">¿Qué esperas para ser parte de nosotros? ¡Únete
                ahora!</a>
        </p>
    </div>
</section>

<section class="bg-primary_800">
    <div class="container-origin py-20">
        <div class="text-white text-5xl font-bold">Noticias destacadas</div>
        <div class="grid grid-cols-2 gap-2 my-5">
            <div class="h-noticias-destacadas bg-cover bg-center bg-no-repeat flex items-end justify-start"
                style="background-image:url('<?= get_stylesheet_directory_uri() ?>/dist/static/noticia_1.png');">
                <div class="text-white p-6">
                    <div class="space-x-5 text-sm">
                        <span class="bg-primary_500 font-bold p-1 px-2">CATEGORIA</span><span>FEBRERO 8, 2022</span>
                    </div>
                    <p class="text-2xl font-bold pt-3">
                        Integer enim neque volutpat ac <br> tincidunt vitae semper
                    </p>
                </div>

            </div>
            <div class="">
                <div class=" h-1/2 bg-cover bg-center pb-2 flex items-end justify-start"
                    style="background-image:url('<?= get_stylesheet_directory_uri() ?>/dist/static/noticia_2.png');">
                    <div class="text-white p-6">
                        <div class="space-x-5 text-sm">
                            <span class="bg-primary_500 font-bold p-1 px-2">CATEGORIA</span><span>FEBRERO 8, 2022</span>
                        </div>
                        <p class="text-lg font-bold pt-3">
                            Integer enim neque volutpat ac tincidunt vitae semper
                        </p>
                    </div>
                </div>
                <div class="h-1/2 pt-2">
                    <div class="grid grid-cols-2 gap-2  h-full">
                        <div class="h-full bg-cover bg-center bg-no-repeat  flex items-end justify-start"
                            style="background-image:url('<?= get_stylesheet_directory_uri() ?>/dist/static/noticia_3.png');">
                            <div class="text-white p-6">
                                <div class="space-x-5 text-sm">
                                    <span class="bg-primary_500 font-bold p-1 px-2">CATEGORIA</span><span>FEBRERO 8,
                                        2022</span>
                                </div>
                                <p class="text-lg font-bold pt-3">
                                    Integer enim neque volutpat ac tincidunt vitae semper
                                </p>
                            </div>
                        </div>
                        <div class="h-full bg-cover bg-center bg-no-repeat flex items-end justify-start"
                            style="background-image:url('<?= get_stylesheet_directory_uri() ?>/dist/static/noticia_4.png');">
                            <div class="text-white p-6">
                                <div class="space-x-5 text-sm">
                                    <span class="bg-primary_500 font-bold p-1 px-2">CATEGORIA</span><span>FEBRERO 8,
                                        2022</span>
                                </div>
                                <p class="text-lg font-bold pt-3">
                                    Integer enim neque volutpat ac tincidunt vitae semper
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
// console.log(WOW);
// window.WOW.init();
</script>

<?= get_footer() ?>