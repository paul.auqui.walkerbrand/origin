<?php
require_once(__DIR__ . '/vendor/autoload.php');
require_once(__DIR__ . '/libs/helper.php');

/**
 * Dump variable.
 */
if (!function_exists('d')) {
	function d()
	{
		call_user_func_array('dump', func_get_args());
	}
}

/**
 * Dump variables and die.
 */
if (!function_exists('dd')) {
	function dd()
	{
		call_user_func_array('dump', func_get_args());
		die();
	}
}

/**
 * Post-thumbnails
 */
function origin_setup()
{
	add_theme_support('post-thumbnails');
}

add_action('after_setup_theme', 'origin_setup');

/**
 * Register Custom Navigation Walker
 */
function register_navwalker()
{
	require_once get_template_directory() . '/libs/navwalker-bootstrap5.php';
}

add_action('after_setup_theme', 'register_navwalker');

register_nav_menus(array(
	'primary' => __('Navegacion Principal', 'Origin'),
	'sidebar' => __('Sidebar Menu', 'Origin'),
));

/**
 * Menu Footer
 */
function menu_footer()
{
	register_sidebar(array(
		'name'          => __('Area 1', 'Origin'),
		'description'   => _('footer - menu'),
		'id'            => 'footer-menu',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h6 class="mb-3 fw-bold widget-title">',
		'after_title'   => '</h6>',
	));

	register_sidebar(array(
		'name'          => __('Area 2', 'Origin'),
		'description'   => _('footer - redessociales'),
		'id'            => 'footer-redessociales',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h6 class="mb-3 fw-bold widget-title">',
		'after_title'   => '</h6>',
	));

	register_sidebar(array(
		'name'          => __('Area 3', 'Origin'),
		'description'   => _('footer - logo'),
		'id'            => 'footer-logo',
		'before_widget' => '<div id="%1$s" class="widget flex justify-start items-center %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h6 class="mb-3 fw-bold widget-title">',
		'after_title'   => '</h6>',
	));
}

add_action('widgets_init', 'menu_footer');


/**
 * Insertar Javascript js y enviar ruta admin-ajax.php
 */
add_action('wp_enqueue_scripts', 'dcms_insertar_js');
function dcms_insertar_js()
{
	/**
	 * 1. Load the datepicker script (pre-registered in WordPress).
	 * 2. Script General
	 */
	wp_enqueue_script('jquery');
	wp_enqueue_script( 'dcms_app_theme', get_stylesheet_directory_uri() . '/dist/js/app.js', [ 'jquery' ], filemtime( get_stylesheet_directory() . '/dist/js/app.js' ) );

}

/**
 *
 */
add_action('admin_enqueue_scripts', 'admin_insertar_js');
function admin_insertar_js()
{
	// wp_enqueue_script('admin_app_theme', get_stylesheet_directory_uri() . '/dist/js/app.js', ['jquery'], '1.0');
	// wp_enqueue_script('my_custom_script', get_stylesheet_directory_uri() . '/src/js/script.js', [], '1.0');

	// wp_localize_script('admin_app_theme', 'dcms_vars', [
	// 	'templateurl' => get_stylesheet_directory_uri()
	// ]);
}

/**
 * Custom logo theme
 */
function origin_custom_logo_setup() {
	$defaults = array(
		'height'               => 100,
		'width'                => 400,
		'flex-height'          => false,
		'flex-width'           => false,
		'header-text'          => array( 'site-title', 'site-description' ),
		'unlink-homepage-logo' => false,
	);

	add_theme_support( 'custom-logo', $defaults );
}

add_action( 'after_setup_theme', 'origin_custom_logo_setup' );