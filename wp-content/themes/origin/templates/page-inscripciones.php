<section class="w-full h-screen bg-primary_700">
    <div class="w-full h-full lg:bg-slider_inscripciones lg:bg-cover lg:bg-center flex justify-center items-center">
        <div class="bg-white p-8 w-2/3 rounded-lg">
            <div class="text-5xl font-bold pb-5">Lorep Ipsum</div>

            <p>
                A tortor eu lacus tincidunt dictumst tincidunt. Natoque malesuada nullam neque, maecenas purus ultrices
                at nunc. Consectetur ac quis a ac egestas odio amet imperdiet. Elit in convallis bibendum hendrerit.
                Blandit aenean sed consectetur facilisis non quam a at. Nunc aliquam fames euismod tristique pulvinar.
                In fames sagittis sem quam. Ullamcorper velit in venenatis, id volutpat laoreet volutpat. Vitae eget
                condimentum bibendum tellus faucibus ut. A sem viverra nunc lobortis habitant a, in.
            </p>
            <p>
                Aliquam, facilisi ut ultricies et lorem duis sed gravida. Gravida nibh eget convallis purus ac
                adipiscing. Eget aliquam nibh ac netus risus in. Pellentesque penatibus massa nullam pellentesque ac
                ultrices donec. Elementum dictum elit ullamcorper sit semper ac dolor. Duis vel sapien eu odio. Praesent
                lorem nisl eget morbi sed arcu, mauris. Quis maecenas vitae non ut nunc, imperdiet ornare sit fusce.
                Morbi bibendum vulputate mattis nunc etiam. Faucibus pretium enim sapien, nulla quam sit. Facilisis
                auctor sagittis leo pharetra, blandit. Varius pretium ornare lorem lorem. Massa egestas pellentesque ac
                non nunc neque, magna venenatis nunc. Amet cursus nisl amet velit, sem velit quam.
            </p>
        </div>
    </div>
</section>

<section class="py-20">
    <div class="container-origin">
        <div class="grid grid-cols-2">
            <div class="">
                <!-- <div class="border h-inscripcion w-full h-full bg-inscripcion bg-cover bg-center" style="ba"></div> -->
                <img class="mx-auto rounded-xl" src="<?= get_stylesheet_directory_uri() ?>/dist/static/inscripcion-image.png"
                    alt="Socio 1">
            </div>
            <div class="">
                <div class="text-5xl font-bold py-4">Inscripción</div>

                <?= do_shortcode( '[forminator_form id="19"]' ); ?>
            </div>
        </div>
    </div>
</section>