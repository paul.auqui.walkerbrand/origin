<section class="w-full vh-75 bg-primary_700">
    <div class="w-full h-full lg:bg-slider_programa lg:bg-cover lg:bg-center">
    </div>
</section>

<section class="py-20">
    <div class="container-origin flex justify-between items-center">
        <div class="w-1/2 ">
            <img class="mx-auto" src="<?= get_stylesheet_directory_uri() ?>/dist/static/que-es-el-programa-image-2.png"
                alt="Socio 1">
        </div>
        <div class="w-1/2 ml-2">
            <div class="text-5xl font-bold py-4">Conoce más sobre Origin</div>
            <p>
                Actualmente contamos con 15 centros de acopio propios y asociados ubicados en Guamaní, Valle de los
                Chillos e Ibarra. Trabajamos día a día por devolver la vida al PET post consumo, colocando en el centro
                al consumidor mediante su decisión de compra final y aportando en el reciclaje de los envases que
                utiliza.
            </p>
            <p class="pt-6 text-primary_500 font-bold text-2xl">
                ¡Sabemos que juntos podemos construir un mundo mejor!
            </p>
        </div>
    </div>
</section>
<section class="bg-bg_section_estadistica bg-cover">
    <div class="container-origin py-20">
        <div class="text-5xl font-bold py-4">Principios</div>
        <p>
            Origin® es una marca de Enkador, empresa que hace 10 años incursionó en el mundo del reciclaje de PET a
            través de la marca Recypet®. El enfoque inicial fue producir resinas plásticas para crear nuevos productos
            como filamento sintético de poliéster, telas y prendas de vestir.
        </p>

        <div class="grid grid-cols-4 gap-8 mt-10">
            <div class="p-8 shadow shadow-sm rounded-lg bg-white">
                <div class="mb-7">
                    <img class="mx-auto"
                        src="<?= get_stylesheet_directory_uri() ?>/dist/static/principios-transformacion-1.png"
                        alt="Transformacion">
                </div>
                <div>
                    <div class="font-bold text-primary_500">Transformación</div>
                    <p>
                        Nuestra producción se centra en lograr la pureza de la materia prima y la inocuidad del producto
                        que llega al consumidor.
                    </p>
                </div>
            </div>
            <div class="p-8 shadow shadow-sm rounded-lg bg-white">
                <div class="mb-7">
                    <img class="mx-auto"
                        src="<?= get_stylesheet_directory_uri() ?>/dist/static/principios-ecodisenio-1.png"
                        alt="Transformacion">
                </div>
                <div>
                    <div class="font-bold text-primary_500">Ecodiseño</div>
                    <p>
                        Creamos envases con capacidad de revivir incontables veces a través del reciclaje.
                    </p>
                </div>
            </div>
            <div class="p-8 shadow shadow-sm rounded-lg bg-white">
                <div class="mb-7">
                    <img class="mx-auto"
                        src="<?= get_stylesheet_directory_uri() ?>/dist/static/principios-produccion-1.png"
                        alt="Transformacion">
                </div>
                <div>
                    <div class="font-bold text-primary_500">Producción con responsabilidad social</div>
                    <p>
                        Mejoramos la calidad de vida de los recicladores de base, reconociendo su oficio y aportando a
                        su productividad y competitividad.

                    </p>
                </div>
            </div>
            <div class="p-8 shadow shadow-sm rounded-lg bg-white">
                <div class="mb-7">
                    <img class="mx-auto"
                        src="<?= get_stylesheet_directory_uri() ?>/dist/static/principios-trazabilidad-1.png"
                        alt="Transformacion">
                </div>
                <div>
                    <div class="font-bold text-primary_500">Trazabilidad</div>
                    <p>
                        Sabemos dónde está el origen de nuestra materia prima y podemos comprobarlo.
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="bg-white py-20 bg-cover bg-center">
    <div class="container-origin">
        <h3 class="font-bold text-5xl mb-6">Nuestra línea de producción</h3>
        <div class="flex justify-between items-center">
            <div class="">
                <img class="mx-auto"
                    src="<?= get_stylesheet_directory_uri() ?>/dist/static/nuestra-linea-produccion.png"
                    alt="Transformacion">
            </div>
            <div class="w-1/3 bg-light_grey p-6 rounded-lg">
                <p class="mb-3">
                    A tortor eu lacus tincidunt dictumst tincidunt. Natoque malesuada nullam neque, maecenas purus
                    ultrices
                    at
                    nunc. Consectetur ac quis a ac egestas odio amet imperdiet. Elit in convallis bibendum hendrerit.
                    Blandit
                    aenean sed consectetur facilisis non quam a at. Nunc aliquam fames euismod tristique pulvinar. In
                    fames
                    sagittis sem quam. Ullamcorper velit in venenatis, id volutpat laoreet volutpat. Vitae eget
                    condimentum
                    bibendum tellus faucibus ut. A sem viverra nunc lobortis habitant a, in.
                </p>
                <p class="mb-3">
                    Aliquam, facilisi ut ultricies et lorem duis sed gravida. Gravida nibh eget convallis purus ac
                    adipiscing.
                    Eget aliquam nibh ac netus risus in. Pellentesque penatibus massa nullam pellentesque ac ultrices
                    donec.
                    Elementum dictum elit ullamcorper sit semper ac dolor. Duis vel sapien eu odio. Praesent lorem nisl
                    eget
                    morbi sed arcu, mauris. Quis maecenas vitae non ut nunc, imperdiet ornare sit fusce. Morbi bibendum
                    vulputate mattis nunc etiam. Faucibus pretium enim sapien, nulla quam sit. Facilisis auctor sagittis
                    leo
                    pharetra, blandit. Varius pretium ornare lorem lorem. Massa egestas pellentesque ac non nunc neque,
                    magna
                    venenatis nunc. Amet cursus nisl amet velit, sem velit quam.
                </p>
            </div>
        </div>
        <div class="bg-primary_500 p-8 rounded-lg text-4xl text-white mt-6 font-bold">
            Cómo se logra observar, nuestro proceso es capaz de aprovechar el envase PET, las tapas y las etiquetas al
            100%.
        </div>
    </div>
</section>
<section class="bg-light_grey">
    <div class="container-origin py-20">
        <div class="text-5xl font-bold">Socios</div>
        <div class="flex justify-between items-center py-14">
            <img src="<?= get_stylesheet_directory_uri() ?>/dist/static/socios_1.png" alt="Socio 1">
            <img src="<?= get_stylesheet_directory_uri() ?>/dist/static/socios_2.png" alt="Socio 1">
            <img src="<?= get_stylesheet_directory_uri() ?>/dist/static/socios_3.png" alt="Socio 1">
            <img src="<?= get_stylesheet_directory_uri() ?>/dist/static/socios_4.png" alt="Socio 1">
        </div>
        <div class="flex justify-between items-center pb-14">
            <img src="<?= get_stylesheet_directory_uri() ?>/dist/static/socios_5.png" alt="Socio 1">
            <img src="<?= get_stylesheet_directory_uri() ?>/dist/static/socios_6.png" alt="Socio 1">
            <img src="<?= get_stylesheet_directory_uri() ?>/dist/static/socios_7.png" alt="Socio 1">
            <img src="<?= get_stylesheet_directory_uri() ?>/dist/static/socios_8.png" alt="Socio 1">
        </div>
        <p>
            Cada vez son más las empresas y personas que están viviendo un despertar en su conciencia y se suman a este
            movimiento. Recordemos que cuidar el planeta es un esfuerzo en el que todos,debemos hacer nuestra parte.
            <a href="#" class="text-primary_500 hover:text-primary_800">¿Qué esperas para ser parte de nosotros? ¡Únete
                ahora!</a>
        </p>
    </div>
</section>

<section class="bg-white">
    <div class="container-origin py-20">
        <div class="text-5xl font-bold">Certificaciones</div>
        <p>
            Contamos con certificaciones a nivel internacional que avalan los procesos que llevamos a cabo en la
            elaboración de nuevos productos con R-PET en Origin.
        </p>

        <div class="grid grid-cols-5 gap-4 pt-8">
            <div class="flex justify-between items-center">
                <img class="mx-auto w-100" src="<?= get_stylesheet_directory_uri() ?>/dist/static/certificado-1.png" alt="Socio 1">
            </div>
            <div class="flex justify-between items-center">
                <img class="mx-auto w-100" src="<?= get_stylesheet_directory_uri() ?>/dist/static/certificado-2.png" alt="Socio 1">
            </div>
            <div class="flex justify-between items-center">
                <img class="mx-auto w-100" src="<?= get_stylesheet_directory_uri() ?>/dist/static/certificado-3.png" alt="Socio 1">
            </div>
            <div class="flex justify-between items-center">
                <img class="mx-auto w-100" src="<?= get_stylesheet_directory_uri() ?>/dist/static/certificado-4.png" alt="Socio 1">
            </div>
            <div class="flex justify-between items-center">
                <img class="mx-auto w-100" src="<?= get_stylesheet_directory_uri() ?>/dist/static/certificado-5.png" alt="Socio 1">
            </div>
        </div>
    </div>
    </div>
</section>