<section class="w-full vh-60 bg-primary_700">
    <div class="w-full h-full lg:bg-slider_blog_1 lg:bg-cover lg:bg-center flex justify-start items-end">

    </div>
</section>

<section class="bg-light_grey">
    <div class="2xl:container flex justify-between items-start space-x-5">
        <div class="w-9/12 transform -translate-y-32">
            <div class="bg-white shadow shadow-lg p-7 rounded-lg">
                <div class="flex items-center space-x-3">
                    <span class="bg-black text-white p-1 px-3 text-sm hover:bg-primary_500 cursor-pointer">ETIQUETA</span>
                    <span class="bg-black text-white p-1 px-3 text-sm hover:bg-primary_500 cursor-pointer">ETIQUETA</span>
                    <span class="bg-black text-white p-1 px-3 text-sm hover:bg-primary_500 cursor-pointer">ETIQUETA</span>
                    <span class="bg-black text-white p-1 px-3 text-sm hover:bg-primary_500 cursor-pointer">ETIQUETA</span>
                    <span class="bg-primary_500 text-white p-1 px-3 text-sm hover:bg-primary_800 cursor-pointer">CATEGORIA</span>
                    <span class="text-sm font-bold">FEBRERO 8, 2022</span>
                </div>
                <h1 class="text-5xl py-8 font-bold">Dale la oportunidad de tener otra vida.</h1>

                <p>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. In nibh leo pretium facilisi eget arcu.
                    Sollicitudin egestas quis eget sed. Lorem tortor, in lorem tortor tincidunt. Neque nunc volutpat
                    turpis
                    nec auctor et mattis. Vel orci quam ultrices nibh arcu sed. In id ut auctor massa ut. Euismod auctor
                    sed
                    fermentum lorem commodo viverra sed ultrices. Faucibus eu aliquam quisque quis mattis.
                    <br>
                    <br>
                    Suspendisse nunc sit nec, varius turpis. Integer lobortis scelerisque eleifend augue egestas at ac
                    egestas sed. Facilisis vel penatibus vitae sollicitudin sed bibendum. Phasellus cras proin pharetra,
                    faucibus suscipit lectus etiam a. Aliquet duis in sit odio quam. Duis purus tortor, et nec purus. Eu
                    mollis tellus lacus volutpat at sapien nec malesuada. Massa eu habitant eget leo leo congue. Egestas
                    quam volutpat proin blandit pretium lacinia. Posuere ipsum suspendisse dui urna, nunc elementum mi
                    facilisis vivamus. Sit suscipit fermentum hac pulvinar molestie odio iaculis malesuada aliquam.
                    Malesuada neque, pellentesque pretium consectetur enim, egestas pulvinar. Volutpat pellentesque mus
                    maecenas urna, et sagittis risus accumsan. Quam adipiscing enim aliquam tincidunt condimentum orci.
                    Fames sit dapibus sem ac semper.
                    <br>
                    <br>
                    Tempor porttitor vulputate tellus cursus. Pulvinar ac, nec non est viverra viverra arcu massa
                    porttitor.
                    Mi amet habitant facilisis vivamus. Ac amet ac feugiat congue varius. Venenatis ipsum eu bibendum
                    ante
                    ac augue at ac. Mattis eros, ornare at nunc ut nec, iaculis eget. At nulla enim ante nibh velit
                    lectus
                    morbi cursus adipiscing.
                    <br>
                    <br>
                    Semper arcu, mauris diam amet egestas blandit orci. Lectus turpis eget non arcu in. Aliquet vel
                    suspendisse turpis vulputate tellus. Est nisi, pellentesque in congue. Urna morbi commodo aliquam
                    consequat ridiculus ut luctus. Nisi, tincidunt dapibus leo enim. Leo, tellus eros, varius eu duis
                    nec.
                    Cursus sit id libero malesuada eget dignissim. Ultricies imperdiet curabitur malesuada lacus. In
                    vulputate curabitur aenean egestas mi malesuada ligula sit. Ipsum lectus diam, sed ultrices in.
                    Lacinia
                    odio adipiscing rhoncus varius suspendisse donec tortor nisl convallis. Ullamcorper viverra non id
                    cursus facilisis. Congue orci et amet vitae viverra malesuada scelerisque.
                </p>
            </div>

            <div class="bg-white shadow shadow-lg p-7 rounded-lg my-8">
                <?= do_shortcode( '[forminator_form id="29"]' ); ?>
            </div>
        </div>

        <div class="shadow shadow-lg p-4 pt-12 w-3/12 space-y-3">
            <div class="h-sidebar bg-cover bg-center bg-no-repeat flex items-end justify-start rounded transform hover:-translate-y-1"
                style="background-image:url('<?= get_stylesheet_directory_uri() ?>/dist/static/noticia_1.png');">
                <div class="text-white p-6">
                    <div class="space-x-5 text-xs">
                        <span class="bg-primary_500 font-bold p-1 px-2">CATEGORIA</span><span>FEBRERO 8, 2022</span>
                    </div>
                    <p class="text-sm font-bold pt-3">
                        Integer enim neque volutpat ac <br> tincidunt vitae semper
                    </p>
                </div>
            </div>

            <div class="h-sidebar bg-cover bg-center bg-no-repeat flex items-end justify-start rounded transform hover:-translate-y-1"
                style="background-image:url('<?= get_stylesheet_directory_uri() ?>/dist/static/noticia_2.png');">
                <div class="text-white p-6">
                    <div class="space-x-5 text-xs">
                        <span class="bg-primary_500 font-bold p-1 px-2">CATEGORIA</span><span>FEBRERO 8, 2022</span>
                    </div>
                    <p class="text-sm font-bold pt-3">
                        Integer enim neque volutpat ac <br> tincidunt vitae semper
                    </p>
                </div>
            </div>

            <div class="h-sidebar bg-cover bg-center bg-no-repeat flex items-end justify-start rounded transform hover:-translate-y-1"
                style="background-image:url('<?= get_stylesheet_directory_uri() ?>/dist/static/noticia_3.png');">
                <div class="text-white p-6">
                    <div class="space-x-5 text-xs">
                        <span class="bg-primary_500 font-bold p-1 px-2">CATEGORIA</span><span>FEBRERO 8, 2022</span>
                    </div>
                    <p class="text-sm font-bold pt-3">
                        Integer enim neque volutpat ac <br> tincidunt vitae semper
                    </p>
                </div>
            </div>

            <div class="h-sidebar bg-cover bg-center bg-no-repeat flex items-end justify-start rounded transform hover:-translate-y-1"
                style="background-image:url('<?= get_stylesheet_directory_uri() ?>/dist/static/noticia_4.png');">
                <div class="text-white p-6">
                    <div class="space-x-5 text-xs">
                        <span class="bg-primary_500 font-bold p-1 px-2">CATEGORIA</span><span>FEBRERO 8, 2022</span>
                    </div>
                    <p class="text-sm font-bold pt-3">
                        Integer enim neque volutpat ac <br> tincidunt vitae semper
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="bg-white">
    <div class="2xl:container p-5">
        <div class="flex justify-between items-center mb-8">
            <h3 class="text-4xl font-bold">Comentarios</h3>
            <div class="bg-light_grey p-3 flex justify-between items-center space-x-2 font-bold">
                <img src="<?= get_stylesheet_directory_uri() ?>/dist/static/icon-coment.png" alt="Icon Coment">
                <span>5.0</span>
                <span class="text-xs">(540 Comentarios)</span>
            </div>
        </div>

        <div class="grid grid-cols-2 gap-5">
            <div class="bg-light_grey p-5 rounded">
                <div class="flex justify-between items-center">
                    <div class="flex items-center space-x-3">
                        <span class="p-3 bg-primary_500 rounded">
                            <img src="<?= get_stylesheet_directory_uri() ?>/dist/static/logo-user.png"
                                alt="Icon Coment">
                        </span>
                        <div>
                            <p class="font-bold text-lg">JUAN GORTAIRE</p>
                            <p class="text-sm">Febrero 2022</p>
                        </div>
                    </div>
                    <div class="flex items-center space-x-3">
                        <img src="<?= get_stylesheet_directory_uri() ?>/dist/static/icon-coment.png" alt="Icon Coment">
                        <span>5.0</span>
                    </div>
                </div>
                <p class="py-5">
                    A peep at some distant orb has power to raise and purify our thoughts like a strain of sacred music,
                    or a noble picture, or a passage from the grander poets. It always does one good...
                </p>
                <a href="#" class="my-4 font-bold hover:text-primary_500 flex items-center space-x-2">
                    <span>Leer completo</span>
                    <img src="<?= get_stylesheet_directory_uri() ?>/dist/static/icon-left.png" alt="Icon Coment">
                </a>
            </div>

            <div class="bg-light_grey p-5 rounded">
                <div class="flex justify-between items-center">
                    <div class="flex items-center space-x-3">
                        <span class="p-3 bg-primary_500 rounded">
                            <img src="<?= get_stylesheet_directory_uri() ?>/dist/static/logo-user.png"
                                alt="Icon Coment">
                        </span>
                        <div>
                            <p class="font-bold text-lg">JUAN GORTAIRE</p>
                            <p class="text-sm">Febrero 2022</p>
                        </div>
                    </div>
                    <div class="flex items-center space-x-3">
                        <img src="<?= get_stylesheet_directory_uri() ?>/dist/static/icon-coment.png" alt="Icon Coment">
                        <span>5.0</span>
                    </div>
                </div>
                <p class="py-5">
                    A peep at some distant orb has power to raise and purify our thoughts like a strain of sacred music,
                    or a noble picture, or a passage from the grander poets. It always does one good...
                </p>
                <a href="#" class="my-4 font-bold hover:text-primary_500 flex items-center space-x-2">
                    <span>Leer completo</span>
                    <img src="<?= get_stylesheet_directory_uri() ?>/dist/static/icon-left.png" alt="Icon Coment">
                </a>
            </div>

            <div class="bg-light_grey p-5 rounded">
                <div class="flex justify-between items-center">
                    <div class="flex items-center space-x-3">
                        <span class="p-3 bg-primary_500 rounded">
                            <img src="<?= get_stylesheet_directory_uri() ?>/dist/static/logo-user.png"
                                alt="Icon Coment">
                        </span>
                        <div>
                            <p class="font-bold text-lg">JUAN GORTAIRE</p>
                            <p class="text-sm">Febrero 2022</p>
                        </div>
                    </div>
                    <div class="flex items-center space-x-3">
                        <img src="<?= get_stylesheet_directory_uri() ?>/dist/static/icon-coment.png" alt="Icon Coment">
                        <span>5.0</span>
                    </div>
                </div>
                <p class="py-5">
                    A peep at some distant orb has power to raise and purify our thoughts like a strain of sacred music,
                    or a noble picture, or a passage from the grander poets. It always does one good...
                </p>
                <a href="#" class="my-4 font-bold hover:text-primary_500 flex items-center space-x-2">
                    <span>Leer completo</span>
                    <img src="<?= get_stylesheet_directory_uri() ?>/dist/static/icon-left.png" alt="Icon Coment">
                </a>
            </div>

            <div class="bg-light_grey p-5 rounded">
                <div class="flex justify-between items-center">
                    <div class="flex items-center space-x-3">
                        <span class="p-3 bg-primary_500 rounded">
                            <img src="<?= get_stylesheet_directory_uri() ?>/dist/static/logo-user.png"
                                alt="Icon Coment">
                        </span>
                        <div>
                            <p class="font-bold text-lg">JUAN GORTAIRE</p>
                            <p class="text-sm">Febrero 2022</p>
                        </div>
                    </div>
                    <div class="flex items-center space-x-3">
                        <img src="<?= get_stylesheet_directory_uri() ?>/dist/static/icon-coment.png" alt="Icon Coment">
                        <span>5.0</span>
                    </div>
                </div>
                <p class="py-5">
                    A peep at some distant orb has power to raise and purify our thoughts like a strain of sacred music,
                    or a noble picture, or a passage from the grander poets. It always does one good...
                </p>
                <a href="#" class="my-4 font-bold hover:text-primary_500 flex items-center space-x-2">
                    <span>Leer completo</span>
                    <img src="<?= get_stylesheet_directory_uri() ?>/dist/static/icon-left.png" alt="Icon Coment">
                </a>
            </div>

            <div class="bg-light_grey p-5 rounded">
                <div class="flex justify-between items-center">
                    <div class="flex items-center space-x-3">
                        <span class="p-3 bg-primary_500 rounded">
                            <img src="<?= get_stylesheet_directory_uri() ?>/dist/static/logo-user.png"
                                alt="Icon Coment">
                        </span>
                        <div>
                            <p class="font-bold text-lg">JUAN GORTAIRE</p>
                            <p class="text-sm">Febrero 2022</p>
                        </div>
                    </div>
                    <div class="flex items-center space-x-3">
                        <img src="<?= get_stylesheet_directory_uri() ?>/dist/static/icon-coment.png" alt="Icon Coment">
                        <span>5.0</span>
                    </div>
                </div>
                <p class="py-5">
                    A peep at some distant orb has power to raise and purify our thoughts like a strain of sacred music,
                    or a noble picture, or a passage from the grander poets. It always does one good...
                </p>
                <a href="#" class="my-4 font-bold hover:text-primary_500 flex items-center space-x-2">
                    <span>Leer completo</span>
                    <img src="<?= get_stylesheet_directory_uri() ?>/dist/static/icon-left.png" alt="Icon Coment">
                </a>
            </div>

            <div class="bg-light_grey p-5 rounded">
                <div class="flex justify-between items-center">
                    <div class="flex items-center space-x-3">
                        <span class="p-3 bg-primary_500 rounded">
                            <img src="<?= get_stylesheet_directory_uri() ?>/dist/static/logo-user.png"
                                alt="Icon Coment">
                        </span>
                        <div>
                            <p class="font-bold text-lg">JUAN GORTAIRE</p>
                            <p class="text-sm">Febrero 2022</p>
                        </div>
                    </div>
                    <div class="flex items-center space-x-3">
                        <img src="<?= get_stylesheet_directory_uri() ?>/dist/static/icon-coment.png" alt="Icon Coment">
                        <span>5.0</span>
                    </div>
                </div>
                <p class="py-5">
                    A peep at some distant orb has power to raise and purify our thoughts like a strain of sacred music,
                    or a noble picture, or a passage from the grander poets. It always does one good...
                </p>
                <a href="#" class="my-4 font-bold hover:text-primary_500 flex items-center space-x-2">
                    <span>Leer completo</span>
                    <img src="<?= get_stylesheet_directory_uri() ?>/dist/static/icon-left.png" alt="Icon Coment">
                </a>
            </div>

            <div class="bg-light_grey p-5 rounded">
                <div class="flex justify-between items-center">
                    <div class="flex items-center space-x-3">
                        <span class="p-3 bg-primary_500 rounded">
                            <img src="<?= get_stylesheet_directory_uri() ?>/dist/static/logo-user.png"
                                alt="Icon Coment">
                        </span>
                        <div>
                            <p class="font-bold text-lg">JUAN GORTAIRE</p>
                            <p class="text-sm">Febrero 2022</p>
                        </div>
                    </div>
                    <div class="flex items-center space-x-3">
                        <img src="<?= get_stylesheet_directory_uri() ?>/dist/static/icon-coment.png" alt="Icon Coment">
                        <span>5.0</span>
                    </div>
                </div>
                <p class="py-5">
                    A peep at some distant orb has power to raise and purify our thoughts like a strain of sacred music,
                    or a noble picture, or a passage from the grander poets. It always does one good...
                </p>
                <a href="#" class="my-4 font-bold hover:text-primary_500 flex items-center space-x-2">
                    <span>Leer completo</span>
                    <img src="<?= get_stylesheet_directory_uri() ?>/dist/static/icon-left.png" alt="Icon Coment">
                </a>
            </div>
        </div>
    </div>
</section>