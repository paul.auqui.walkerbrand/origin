<section class="w-full h-screen bg-primary_700">
    <div class="w-full h-full lg:bg-slider_contactos_1 lg:bg-cover lg:bg-center">
        <div class="container-origin w-full h-full flex justify-between items-center space-x-5">
            <img src="<?= get_stylesheet_directory_uri() ?>/dist/static/logo-contactos.png" alt="Contactos">
            <div class="bg-white rounded p-5 w-8/12">
                <div class="text-xs font-bold relative pl-3 af-contactos">
                    <p>Quito - Ecuador</p>
                    <p>Dirección</p>
                    <p>Teléfono</p>
                    <p>Correo electrónico</p>
                </div>
                <?= do_shortcode( '[forminator_form id="32"]' ); ?>
            </div>
        </div>
    </div>
</section>