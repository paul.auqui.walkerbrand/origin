<?php
$helper      = new helper();
$banner_id   = get_the_ID();
$imagebg     = $helper->getImageMeta( $banner_id, 'backgroud_image', 'full' );
$description = get_post_meta( $banner_id, 'descripcion', true ) ?? null;
?>

<div class="container-fluid mx-0 p-0">
    <div class="contenedor-banner normal relative" style="background: url(<?= $imagebg ?>);">
        <div class="container h-100">
            <div class="row h-100">
                <div class="col-12 col-md-12 col-xl-8  d-flex justify-content-start align-items-end mb-4">
                    <div class="enlaces-banner home-hr px-3 py-4">
						<?= get_template_part( 'templates/partials/menu-seguros-home-hr', '', [
							'post_seguro_id' => 0,
							'class-menu'     => 'seguros',
						] ) ?>
                    </div>
                </div>
                <div class="col-12 col-md-12 col-xl-4 d-md-flex d-md-none d-xl-flex d-none justify-content-end align-items-center">
                    <div class="intro-banner-textoHome text-end">
						<?= $description ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>