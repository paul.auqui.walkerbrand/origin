<section class="w-full h-screen bg-primary_700 lg:bg-slider_blog lg:bg-cover lg:bg-center">
    <div class="container-origin w-full h-full flex justify-end items-center flex-col">
        <div class="flex space-x-8 mb-10 w-full text-3xl">
            <div class="text-primary_500 font-bold w-1/2 text-right">RECICLAR BIEN</div>
            <div class="text-dark_grey_2 font-bold w-1/2">TRANSFORMA VIDAS</div>
        </div>
        <div class="bg-white p-8 w-10/12 rounded-lg mb-8">
            <div class="flex space-x-4 items-center mb-3">
                <span class="bg-primary_500 text-sm text-white font-bold p-1 px-3">CATEGORIA</span>
                <span class="font-bold text-sm">Febrero 8, 2022</span>
            </div>
            <div class="text-5xl font-bold mb-3">Origin - El poder de uno.</div>
            <p class="pb-3 text-sm">
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Lectus integer quis orci est quis nulla mauris
                hendrerit sed. Eu, molestie rhoncus nullam quisque in adipiscing donec tortor. Tincidunt congue enim....
            </p>
            <button class="btn-ver-mas w-auto p-2">Leer artículo completo</button>
        </div>
    </div>
</section>

<section class="py-20">
    <div class="container-origin">
        <div class="flex items-center  mb-8">
            <div class="w-10/12 pr-8">
                <input
                    class="w-full p-2 rounded border border-grey shadow shadow-sm focus:border-grey focus:bg-light_grey"
                    type="text" name="" id="" placeholder="Buscar">
            </div>
            <div class="">
                <select class="w-32 bg-light_grey p-2 rounded" name="" id="">
                    <option value="Cat 1">Cat 1</option>
                    <option value="Cat 1">Cat 2</option>
                </select>
            </div>
            <div class="text-center  mx-auto">
                <button class="bg-light_grey p-2 hover:bg-primary_500 rounded">
                    <img src="<?= get_stylesheet_directory_uri() ?>/dist/static/filter.png" alt="Socio 1">
                </button>
            </div>
        </div>
        <div class="grid grid-cols-2 gap-2">
            <div class="h-blog bg-cover bg-center bg-no-repeat flex items-end justify-start rounded transform hover:-translate-y-1"
                style="background-image:url('<?= get_stylesheet_directory_uri() ?>/dist/static/noticia_1.png');">
                <div class="text-white p-6">
                    <div class="space-x-5 text-sm">
                        <span class="bg-primary_500 font-bold p-1 px-2">CATEGORIA</span><span>FEBRERO 8, 2022</span>
                    </div>
                    <p class="text-2xl font-bold pt-3">
                        Integer enim neque volutpat ac <br> tincidunt vitae semper
                    </p>
                </div>
            </div>
            <div class="h-blog bg-cover bg-center bg-no-repeat flex items-end justify-start rounded transform hover:-translate-y-1"
                style="background-image:url('<?= get_stylesheet_directory_uri() ?>/dist/static/noticia_2.png');">
                <div class="text-white p-6">
                    <div class="space-x-5 text-sm">
                        <span class="bg-primary_500 font-bold p-1 px-2">CATEGORIA</span><span>FEBRERO 8, 2022</span>
                    </div>
                    <p class="text-2xl font-bold pt-3">
                        Integer enim neque volutpat ac <br> tincidunt vitae semper
                    </p>
                </div>
            </div>
            <div class="h-blog bg-cover bg-center bg-no-repeat flex items-end justify-start rounded transform hover:-translate-y-1"
                style="background-image:url('<?= get_stylesheet_directory_uri() ?>/dist/static/noticia_3.png');">
                <div class="text-white p-6">
                    <div class="space-x-5 text-sm">
                        <span class="bg-primary_500 font-bold p-1 px-2">CATEGORIA</span><span>FEBRERO 8, 2022</span>
                    </div>
                    <p class="text-2xl font-bold pt-3">
                        Integer enim neque volutpat ac <br> tincidunt vitae semper
                    </p>
                </div>
            </div>
            <div class="h-blog bg-cover bg-center bg-no-repeat flex items-end justify-start rounded transform hover:-translate-y-1"
                style="background-image:url('<?= get_stylesheet_directory_uri() ?>/dist/static/noticia_4.png');">
                <div class="text-white p-6">
                    <div class="space-x-5 text-sm">
                        <span class="bg-primary_500 font-bold p-1 px-2">CATEGORIA</span><span>FEBRERO 8, 2022</span>
                    </div>
                    <p class="text-2xl font-bold pt-3">
                        Integer enim neque volutpat ac <br> tincidunt vitae semper
                    </p>
                </div>
            </div>
            <div class="h-blog bg-cover bg-center bg-no-repeat flex items-end justify-start rounded transform hover:-translate-y-1"
                style="background-image:url('<?= get_stylesheet_directory_uri() ?>/dist/static/noticia_5.png');">
                <div class="text-white p-6">
                    <div class="space-x-5 text-sm">
                        <span class="bg-primary_500 font-bold p-1 px-2">CATEGORIA</span><span>FEBRERO 8, 2022</span>
                    </div>
                    <p class="text-2xl font-bold pt-3">
                        Integer enim neque volutpat ac <br> tincidunt vitae semper
                    </p>
                </div>
            </div>
            <div class="h-blog bg-cover bg-center bg-no-repeat flex items-end justify-start rounded transform hover:-translate-y-1"
                style="background-image:url('<?= get_stylesheet_directory_uri() ?>/dist/static/noticia_6.png');">
                <div class="text-white p-6">
                    <div class="space-x-5 text-sm">
                        <span class="bg-primary_500 font-bold p-1 px-2">CATEGORIA</span><span>FEBRERO 8, 2022</span>
                    </div>
                    <p class="text-2xl font-bold pt-3">
                        Integer enim neque volutpat ac <br> tincidunt vitae semper
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>