<?php
$helper = new helper();

$vacantes = $helper->getPostTypeVacantes();
if ( $vacantes ):
	?>
    <div class="d-flex justify-content-center mt-5">
        <h3 class="text-center fw-bold text-black "><?= __( 'Vacantes' ) ?></h3>
    </div>

    <div class="slide-vacantes row d-flex">
		<?php
		foreach ( $vacantes as $vacante ):
			?>
            <div class="col-md-4 mt-4 d-md-flex align-items-between">
                <div class="box-vacantes px-4 pt-4 pb-2 d-flex justify-content-between flex-column">
                    <div class="h6 text-black fw-bold pb-2">
						<?= $vacante->title ?>
                    </div>
                    <div class="d-flex align-items-start flex-column anuncio">
						<?= $vacante->anuncio ?>
                    </div>
                    <div class="anuncio">
                        <ul class="ciudad ">
                            <li><strong><?= $vacante->ciudad ?></strong></li>
                        </ul>
                    </div>

                    <div class="d-flex justify-content-center">
                        <a href="<?= $vacante->link ?>" class="nav-link"><?= __( 'APLICAR' ) ?></a>
                    </div>
                </div>
            </div>
		<?php
		endforeach;
		?>
    </div>
<?php else: ?>

<?php endif; ?>

<div class="container">
    <div class="row">
        <div class="col h3 text-center fw-bold py-4">
			<?= __( 'Aplica aquí' ) ?>
        </div>
    </div>

    <div class="row d-none">
        <div class="col">
            <img src="<?= get_stylesheet_directory_uri() ?>/dist/static/information-outline-yellow.png" alt="">
            <span class="mx-2 text-muted fst-italic">
                <?= __( 'Al momento no tenemos vacantes disponibles, pero estaremos gustosos de recibir tu CV para futuras contrataciones, deja tus datos y hoja de vida aquí:' ) ?>
            </span>
        </div>
    </div>
</div>

<section class="bg-aplicar py-3">
    <form id="form-vacantes" action="dcms_ajax_aplicacion_vacantes" enctype="multipart/form-data" method="post"
          novalidate>
        <input type="hidden" name="CPT" value="aplicacion-vacante">
        <input type="hidden" name="msn" value="vacante">
        <input type="hidden" name="aplicacion_fecha" value="<?= date( 'Y-m-d h:i' ) ?>">
        <div class="container">
            <div class="row pb-3">
                <div class="col">
                    <div class="form-group">
                        <label for="cargo_aplicar" class="fw-bold">Profesi&oacute;n</label>
                        <input type="text" class="form-control" id="profesion" name="profesion"
                               aria-describedby="Profesión aplicar"
                               placeholder="Ingresa tu profesión" required>
                    </div>
                </div>
            </div>
			<?php get_template_part( 'templates/partials/form_vacantes' ) ?>
        </div>
    </form>
</section>


