<?php
	global $pagename;

	get_header();
	the_post();
	get_template_part( 'templates/page', $pagename );
	get_footer();
?>