<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<?php
$search = get_the_permalink();
?>

<head>
    <title>
        <?= bloginfo( 'name' ); ?> &raquo; <?= is_front_page() ? bloginfo( 'description' ) : wp_title( '' ); ?>
    </title>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="profile" href="http://gmpg.org/xfn/11">
    <link rel="icon" href="<?= get_site_icon_url( 512 ) ?>" />
    <link rel="icon" href="<?= get_site_icon_url( 192 ) ?>" />
    <link rel="icon" href="<?= get_site_icon_url( 32 ) ?>" />

    <link rel="preconnect" href="https://fonts.googleapis.com">

    <link
        href="https://fonts.googleapis.com/css2?family=Source+Sans+Pro:ital,wght@0,200;0,300;0,400;0,600;0,700;0,900;1,200;1,300;1,400;1,600;1,700;1,900&display=swap"
        rel="stylesheet">

    <!-- 
        <link
        href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&family=Source+Sans+Pro:ital,wght@0,200;0,300;0,400;0,600;0,700;0,900;1,200;1,300;1,400;1,600;1,700;1,900&display=swap"
        rel="stylesheet" /> 
    -->

    <link rel="stylesheet"
        href="<?= get_stylesheet_directory_uri() ?>/dist/css/app.css?v=<?= filemtime( get_stylesheet_directory() . '/dist/css/app.css' ) ?>"
        type="text/css" media="screen, projection" />

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css" />

    <?php wp_head() ?>
</head>

<body>
    <nav id="menu-principal" class="fixed w-full px-2 sm:px-4 pt-4 text-white">
        <div class="container-origin flex flex-wrap justify-between items-center">
            <?php
			if ( function_exists( 'the_custom_logo' ) ) {
				the_custom_logo();
			}
			?>

            <div class="md:flex justify-between items-center space-x-4 w-full md:w-auto md:order-1" id="mobile-menu-2">
                <?php
				wp_nav_menu( [
					'theme_location'  => 'primary',
					'depth'           => 2,
					'container'       => 'div',
					'container_class' => 'collapse navbar-collapse nav-principal',
					'container_id'    => 'nav-principal',
					'menu_class'      => 'flex flex-col mt-4 md:flex-row md:space-x-8 xl:space-x-16 md:mt-0 md:text-sm md:font-medium',
					'fallback_cb'     => '__return_false',
					'items_wrap'      => '<ul id="%1$s" class="navbar-nav me-auto mb-2 mb-md-0 %2$s">%3$s</ul>',
					'walker'          => new bootstrap_5_wp_nav_menu_walker()
				] );
				?>
            </div>
        </div>
    </nav>