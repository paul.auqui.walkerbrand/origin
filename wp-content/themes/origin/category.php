<?php
/**
 * Created by PhpStorm.
 * User: ZBOOK
 * Date: 18/05/2021
 * Time: 12:09
 */
$home = get_page_by_path( 'tecniblog' );
get_header();

get_template_part( 'templates/banner/tecniblog', null, [ 'home' => $home ] );

?>
<?= get_template_part( 'templates/partials/posts-loop' ) ?>
<?php get_footer();?>